<?php
    //DBとの連携確認
     $link = mysqli_connect("localhost","root","root","mindMap");
    //サーバー名(ホスト)、データベースユーザ名、パスワード,データベース名
    if(mysqli_connect_error()){
        die("DBへの接続に失敗しました");
    }
    
    session_start();
    
    if(array_key_exists('title',$_POST)){
        $_SESSION["title"] = $_POST['title'];
        header("Location:mindMapSelect.php");
    }

?>



<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        
        <title>マインドマップ</title>
    </head>

    <body>
        <h1>マインドマップ</h1>
        
        
        <P></P>
        <a class="btn btn-primary" href="mindMapEdit.php" role="button">新しく作る</a>
        
        <div class="container">
            <form method="post">
                <div class="form-group">
                    <label for="exampleInputEmail1">タイトルで検索（or検索）</label>
                    <input type="テキスト" class="form-control" aria-describedby="emailHelp" name="title" placeholder="タイトルに関連する単語を入力">
                    <button type="submit" class="btn btn-primary">検索する</button>
                </div>
            </form>
        </div>
        
        
    
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    </body>
</html>